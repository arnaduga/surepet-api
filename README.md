# SurePet API - Reverse engineering

Result of the reverse engineering of the Sur Pet API

***Help Wanted***


# What is it?

This project is *just* an [Open API Specification](https://www.openapis.org/) document, to describe the SurePet API.

This API is not publicly documented. This is a tentative of reverse engineering it, in order to be able to integrate it then into home automation projects.

# I want to help!

SURE! I am willing to get some help on this reverse engineering task. I mainly focus on **GET** actions for the moment, but I need help to:
- identify other endpoints
- identify other VERBs

