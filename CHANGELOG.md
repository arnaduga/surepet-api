# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 1.0.5 // 2021-04-21

### Changed
- CICD pipeline to build different page
- Page based on Swagger UI and not ReDoc anymore

## 1.0.4 // 2021-04-18

No change between 1.0.3, except CHANGELOG update ;-)

### Added
- Project creation
- ```CHANGELOG.md``` file
- ```README.md```file
- First *version* of the OAS3 file
- Postman collection and environment file, after secrets cleanup
- Gitlab CICD pipeline information file
- Improves OAS file
- Export POSTMAN objects
- Logo sources and export PNG
- ... final release! :)


### Fixed
- Missing authN for one endpoint
- CICD issues
