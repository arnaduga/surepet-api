openapi: 3.0.0

info:
  title: SurePet API - Reverse engineering
  version: "1.0"
  x-logo:
    url: "./SurePetAPI.png"
    altText: "SurePet API"  
  description: |
    # SurePet API
    
    **_WARNING_**: This document is NOT official from the API owner. Use it at your own risk related to the owner and its usage conditions.

    ## API Purpose

    THis API is the one used by the mobile application [Sure PetCare]() and the website [Sure PetCare](https://www.surepetcare.io) to have a look on your Sure Pet devices.

    Devices you can handle thanks to this API are various: hub, petdoor, petfeeder, etc. This _OpenAPI specification_ file intend to document the API. However, it has been built by using only the hub and petdoor. That could explain why there is no such API example with feeder machine.


    ### Why?

    This reverse engineering process has been initiated to fill the gap between the application capabilities and my expectations.

    For instance, I wanted the petdoor to be locked on ingress (to allow to get out, but not get) between 6am and 8am, to be sure the cat won't came into our bedroom to beg some food. This was not possible with the application.

    So, by using a [Raspberry Pi](https://www.raspberrypi.org/) and [Node-Red](https://nodered.org/), I needed then the API to do the job.

    Another needs was the ability to get the cat position (inside or outside the home), to display the information thanks to an eInk display and Raspberry Pi Zero.

    ## Postman Collection

    Yes, I have that too! :)

    The POSTMAN collection can be found [HERE](https://gitlab.com/arnaduga/surepet-api/-/blob/master/SureHub%20API.postman_collection.json) and the associated environment definition [HERE](https://gitlab.com/arnaduga/surepet-api/-/blob/master/SureHubEnv.postman_environment.json). Enjoy!

    ## Contributing

    This API description is NOT complete for sure. May you wants to contribute, feel free to fork the [repository](https://gitlab.com/arnaduga/surepet-api) and create pull requests!

  contact:
    name: Arnaud
    email: arnauduga@gmail.com
  license:
    name: MIT

servers:
  - url: "https://app.api.surehub.io/api"



paths:
  /auth/login:
    post:
      summary: Post authentication credentials
      tags:
        - authentication
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                title: token
                type: object
                properties:
                  data:
                    type: object
                    properties:
                      user:
                        type: object
                        properties:
                          id:
                            type: integer
                          email_address:
                            type: string
                          first_name:
                            type: string
                          last-name:
                            type: string
                          country_id:
                            type: integer
                          language_id:
                            type: integer
                          marketing_opt_in:
                            type: boolean
                          terms_accepted:
                            type: boolean
                          weight_units:
                            type: integer
                          time_format:
                            type: integer
                          version:
                            type: string
                          created_at:
                            type: string
                          updated_at:
                            type: string
                          notifications:
                            type: object
                            properties:
                              device_status:
                                type: boolean
                              animal_movement:
                                type: boolean
                              intruder_movements:
                                type: boolean
                              new_device_pet:
                                type: boolean
                              household_management:
                                type: boolean
                              photos:
                                type: boolean
                              low_battery:
                                type: boolean
                              curfew:
                                type: boolean
                              feeding_activity:
                                type: boolean
                      token:
                        type: string
                x-examples:
                  Success:
                    data:
                      user:
                        id: 12345
                        email_address: email@domain.com
                        first_name: Robert
                        last_name: Mac
                        country_id: 75
                        language_id: 47
                        marketing_opt_in: false
                        terms_accepted: true
                        weight_units: 0
                        time_format: 0
                        version: Mg==
                        created_at: "2019-03-12T19:55:42+00:00"
                        updated_at: "2020-01-01T11:22:33+00:00"
                        notifications:
                          device_status: true
                          animal_movement: true
                          intruder_movements: true
                          new_device_pet: true
                          household_management: true
                          photos: true
                          low_battery: true
                          curfew: true
                          feeding_activity: true
                      token: eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2FwcC5hcGkuc3VyZWh1Yi5pby9hcGkvYXV0aC9sb2dpbiIsImlhdCI6MTU4NTA0NDU3MywiZXhwIjoxNjE2NDk0MTczLCJuYmYiOjE1ODUwNDQ1NzMsImp0aSI6InJGRGVlWnNhWmVsSzk4dVkiLCJzdWIiOjEyMzQ1LCJwcnYiOiJlZmNiNTQzMmVhYWJjZGVmMTIzNDAxMjM0NTY3ODlhYm5jZGVmOGE3ZCIsImRldmljZV9pZCI6IjQyNTM0MTcyNjMifQ.rMz49zp6R5i9WB27z6RiJYMzTmGwLz1D1sWEF9FeKvY
                description: "Login feedback, containing the precious TOKEN, authentication sesame to all th other API endpoints"
              examples:
                example:
                  value:
                    data:
                      user:
                        id: 12345
                        email_address: email@domain.com
                        first_name: John
                        last_name: Smith
                        country_id: 75
                        language_id: 47
                        marketing_opt_in: false
                        terms_accepted: true
                        weight_units: 0
                        time_format: 0
                        version: Mg==
                        created_at: "2019-10-19T07:21:23+00:00"
                        updated_at: "2020-02-01T12:33:23+00:00"
                        notifications:
                          device_status: true
                          animal_movement: true
                          intruder_movements: true
                          new_device_pet: true
                          household_management: true
                          photos: true
                          low_battery: true
                          curfew: true
                          feeding_activity: true
                      token: eeyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2FwcC5hcGkuc3VyZWh1Yi5pby9hcGkvYXV0aC9sb2dpbiIsImlhdCI6MTU4NTA0NDU3MywiZXhwIjoxNjE2NDk0MTczLCJuYmYiOjE1ODUwNDQ1NzMsImp0aSI6ImFnaXVPWmZ6cTNBR0lVOTQiLCJzdWIiOjUzMTgyLCJwcnYiOiJiM2VkZWRkMzM0YzJiYzMzNjU4NDI2OTRhYmNkNTZhM2ZmYmY1YTdkIiwiZGV2aWNlX2lkIjoiMTIxMjEyMTIxMiJ9.zUYjzex7FNCQEwWDs4-KJ7hJbJsbB3mKPsUrLaCQ4yA
          headers:
            Authorization:
              schema:
                type: string
        "401":
          description: Unauthorized
          headers:
            Content-Type:
              schema:
                type: string
          content:
            application/json:
              schema:
                type: object
                properties:
                  error:
                    type: object
                    properties:
                      login:
                        type: array
                        items:
                          type: string
              examples:
                authN_error:
                  value:
                    error:
                      login:
                        - invalid_email_or_password
      operationId: post-auth-login
      parameters: []
      requestBody:
        description: ""
        content:
          application/json:
            schema:
              title: authentication
              type: object
              description: '{"email_address": user, "password": passwd, "device_id": devid}'
              properties:
                email_address:
                  type: string
                password:
                  type: string
                device_id:
                  type: string
                  description: A device identifier. Cannot be `null` but can be "."

      description: |-
        Get the token information required to make any other endpoint calls.

        Please note the Token is part of the body response and in header `Authorization`.
  /pet:
    get:
      summary: Your GET endpoint
      tags:
        - pet
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  data:
                    type: array
                    items:
                      title: pets
                      type: object
                      properties:
                        updated_at:
                          type: string
                        created_at:
                          type: string
                        version:
                          type: string
                        tag_id:
                          type: integer
                        species_id:
                          type: integer
                        photo_id:
                          type: integer
                        household_id:
                          type: integer
                        comments:
                          type: string
                        date_of_birth:
                          type: string
                        gender:
                          type: integer
                        name:
                          type: string
                        id:
                          type: integer
      operationId: get-pet
      description: Retrieve the list of all the pets registered.
      security:
        - BearerToken: []
  /start:
    get:
      summary: Your GET endpoint
      tags:
        - helper
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  data:
                    type: object
                    properties:
                      breed:
                        type: array
                        items:
                          title: breed
                          type: object
                          properties:
                            id:
                              type: integer
                            name:
                              type: string
                            species_id:
                              type: integer
                            version:
                              type: string
                      condition:
                        type: array
                        items:
                          title: condition
                          type: object
                          properties:
                            id:
                              type: integer
                            name:
                              type: string
                            version:
                              type: string
                      country:
                        type: array
                        items:
                          title: country
                          type: object
                          properties:
                            id:
                              type: integer
                            name:
                              type: string
                            code:
                              type: string
                            default_language_id:
                              type: integer
                            created_at:
                              type: string
                            updated_at:
                              type: string
                      food_type:
                        type: array
                        items:
                          title: food_type
                          type: object
                          properties:
                            id:
                              type: integer
                            name:
                              type: string
                            version:
                              type: string
                      language:
                        type: array
                        items:
                          title: language
                          type: object
                          properties:
                            id:
                              type: integer
                            name:
                              type: string
                            native_name:
                              type: string
                            code:
                              type: string
                            created_at:
                              type: string
                            updated_at:
                              type: string
                      product:
                        type: array
                        items:
                          title: product
                          type: object
                          properties:
                            id:
                              type: integer
                            name:
                              type: string
                            version:
                              type: string
                            created_at:
                              type: string
                            updated_at:
                              type: string
                      species:
                        type: array
                        items:
                          title: species
                          type: object
                          properties:
                            id:
                              type: integer
                            name:
                              type: string
                            version:
                              type: string
                      timezone:
                        type: array
                        items:
                          title: timezone
                          type: object
                          properties:
                            id:
                              type: integer
                            name:
                              type: string
                            timezone:
                              type: string
                            utc_offset:
                              type: string
                            created_at:
                              type: string
                            updated_at:
                              type: string
                      translation:
                        type: array
                        items:
                          type: object
                      version:
                        title: version
                        type: object
                        properties:
                          server:
                            type: string
                          app:
                            type: object
                            properties:
                              ios:
                                type: object
                                properties:
                                  min_version:
                                    type: object
                                    properties:
                                      major:
                                        type: string
                                      minor:
                                        type: string
                                  store_link:
                                    type: string
                              android:
                                type: object
                                properties:
                                  min_version:
                                    type: object
                                    properties:
                                      major:
                                        type: string
                                      minor:
                                        type: string
                                  store_link:
                                    type: string
                        x-examples:
                          example-1:
                            server: 1.0.1-35b2388
                            app:
                              ios:
                                min_version:
                                  major: "1.0"
                                  minor: "1.17"
                                store_link: "https://itunes.apple.com/gb/app/surehub/id1036175339?mt=8"
                              android:
                                min_version:
                                  major: 1
                                  minor: 0
                                store_link: "https://play.google.com/store/apps/details?id=com.sureflap.surepetcare"                          version: MA==
                          version: MA==
      description: |-
        Get different information for displaying and filtergin, like language id, timezones, species, etc.

        Total size is approx. 235Kb. All the information are supposed to be static (except vendor update).
      security:
        - BearerToken: []
  /device:
    get:
      summary: Get list of devices related to the account
      tags:
        - device
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  data:
                    type: array
                    items:
                      title: device
                      type: object
                      properties:
                        id:
                          type: string
                        product_id:
                          type: integer
                          description: |-
                            * 1 = Hub
                            * 6 = Connected catdoor
                        household_id:
                          type: integer
                        name:
                          type: string
                        serial_number:
                          type: string
                        mac_address:
                          type: string
                        version:
                          type: string
                        created_at:
                          type: string
                        updated_at:
                          type: string
                        parent_device_id:
                          type: string
                          description: "Optional: not for HUB"
                        index:
                          type: string
                          description: Optional. Not for HUB
      description: Get list of devices related to the account
      security:
        - BearerToken: []
  /photo:
    get:
      summary: Get your picture pet details
      tags:
        - pet
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  data:
                    type: array
                    items:
                      title: photo
                      type: object
                      properties:
                        id:
                          type: integer
                        location:
                          type: string
                        uploading_user_id:
                          type: integer
                        version:
                          type: string
                        created_at:
                          type: string
                        updated_at:
                          type: string
      description: Get information about posted photos
      security:
        - BearerToken: []
  /pet/:id:
    parameters:
      - schema:
          type: string
        name: id
        in: path
        required: true
        description: PET id
    get:
      summary: Get your pet details
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  data:
                    title: pets
                    type: object
                    properties:
                      updated_at:
                        type: string
                      created_at:
                        type: string
                      version:
                        type: string
                      tag_id:
                        type: integer
                      species_id:
                        type: integer
                      photo_id:
                        type: integer
                      household_id:
                        type: integer
                      comments:
                        type: string
                      date_of_birth:
                        type: string
                      gender:
                        type: integer
                      name:
                        type: string
                      id:
                        type: integer
                  pe:
                    type: string
      description: "Retrieve one pet details, based on his id"
      security:
        - BearerToken: []
      tags:
        - pet
  /pet/{id}/position:
    parameters:
      - schema:
          type: string
        name: id
        in: path
        required: true
        description: PET id
    get:
      summary: Get your pet position
      tags:
        - pet
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  data:
                    title: position
                    type: object
                    properties:
                      pet_id:
                        type: integer
                      tag_id:
                        type: integer
                      device_id:
                        type: integer
                      where:
                        type: integer
                      since:
                        type: string
                    description: ""
                  "":
                    type: string
      description: Retrieve information about the pet position (inside or outside)
      security:
        - BearerToken: []
    post:
      summary: "Set your pet position"
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  where:
                    type: string
                    description: Value `1` for INSIDE, `2` for OUTSIDE
                  since:
                    type: string
                    description: 'Date of action, format = "YYYY-MM-DD hh:mm:ss"'
      description: Update the pet position
      tags:
        - pet
      security:
        - BearerToken: []

  /device/{id}/control:
    parameters:
      - schema:
          type: string
        name: id
        in: path
        required: true
    get:
      summary: Your device endpoint
      tags:
        - device
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  data:
                    type: object
                    properties:
                      led_mode:
                        type: integer
                      pairing_mode:
                        type: integer
      operationId: get-device-id-control
      description: Get some details about a device
      security:
        - BearerToken: []
  /me/client:
    get:
      summary: Get information about your account
      tags:
        - helper
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  data:
                    type: array
                    items:
                      type: object
                      properties:
                        id:
                          type: integer
                        user_id:
                          type: integer
                        platform:
                          type: object
                          properties:
                            app:
                              type: object
                              properties:
                                bundle_identifier:
                                  type: string
                                version:
                                  type: string
                            device:
                              type: object
                              properties:
                                model:
                                  type: object
                                  properties:
                                    manufacturer:
                                      type: string
                                    name:
                                      type: string
                                os:
                                  type: object
                                  properties:
                                    platform:
                                      type: string
                                    version:
                                      type: string
                                uuid:
                                  type: string
                        version:
                          type: string
                        created_at:
                          type: string
                        updated_at:
                          type: string
                      description: ""
      operationId: get-me-client
      description: Gt some details about the connected user (from token)
      security:
        - BearerToken: []
components:
  schemas:
    authentication:
      title: authentication
      type: object
      description: '{"email_address": user, "password": passwd, "device_id": devid}'
      properties:
        email_address:
          type: string
        password:
          type: string
        device_id:
          type: string
    token:
      title: token
      type: object
      properties:
        data:
          type: object
          properties:
            user:
              type: object
              properties:
                id:
                  type: integer
                email_address:
                  type: string
                first_name:
                  type: string
                last-name:
                  type: string
                country_id:
                  type: integer
                language_id:
                  type: integer
                marketing_opt_in:
                  type: boolean
                terms_accepted:
                  type: boolean
                weight_units:
                  type: integer
                time_format:
                  type: integer
                version:
                  type: string
                created_at:
                  type: string
                updated_at:
                  type: string
                notifications:
                  type: object
                  properties:
                    device_status:
                      type: boolean
                    animal_movement:
                      type: boolean
                    intruder_movements:
                      type: boolean
                    new_device_pet:
                      type: boolean
                    household_management:
                      type: boolean
                    photos:
                      type: boolean
                    low_battery:
                      type: boolean
                    curfew:
                      type: boolean
                    feeding_activity:
                      type: boolean
            token:
              type: string
      x-examples:
        Success:
          data:
            user:
              id: 12345
              email_address: email@domain.com
              first_name: Robert
              last_name: Mac
              country_id: 75
              language_id: 47
              marketing_opt_in: false
              terms_accepted: true
              weight_units: 0
              time_format: 0
              version: Mg==
              created_at: "2019-03-12T19:55:42+00:00"
              updated_at: "2020-01-01T11:22:33+00:00"
              notifications:
                device_status: true
                animal_movement: true
                intruder_movements: true
                new_device_pet: true
                household_management: true
                photos: true
                low_battery: true
                curfew: true
                feeding_activity: true
            token: <token>
      description: "Login feedback, containing the precious TOKEN, authentication sesame to all th other API endpoints"
    pet:
      title: pets
      type: object
      properties:
        updated_at:
          type: string
        created_at:
          type: string
        version:
          type: string
        tag_id:
          type: integer
        species_id:
          type: integer
        photo_id:
          type: integer
        household_id:
          type: integer
        comments:
          type: string
        date_of_birth:
          type: string
        gender:
          type: integer
        name:
          type: string
        id:
          type: integer
    client:
      type: object
      properties:
        id:
          type: integer
        user_id:
          type: integer
        platform:
          type: object
          properties:
            app:
              type: object
              properties:
                bundle_identifier:
                  type: string
                version:
                  type: string
            device:
              type: object
              properties:
                model:
                  type: object
                  properties:
                    manufacturer:
                      type: string
                    name:
                      type: string
                os:
                  type: object
                  properties:
                    platform:
                      type: string
                    version:
                      type: string
                uuid:
                  type: string
        version:
          type: string
        created_at:
          type: string
        updated_at:
          type: string
      description: ""
    device:
      title: device
      type: object
      properties:
        id:
          type: string
        product_id:
          type: integer
          description: |-
            * 1 = Hub
            * 6 = Connected catdoor
        household_id:
          type: integer
        name:
          type: string
        serial_number:
          type: string
        mac_address:
          type: string
        version:
          type: string
        created_at:
          type: string
        updated_at:
          type: string
        parent_device_id:
          type: string
          description: "Optional: not for HUB"
        index:
          type: string
          description: "Optional: not for HUB"
    breed:
      title: breed
      type: object
      properties:
        id:
          type: integer
        name:
          type: string
        species_id:
          type: integer
        version:
          type: string
    condition:
      title: condition
      type: object
      properties:
        id:
          type: integer
        name:
          type: string
        version:
          type: string
    country:
      title: country
      type: object
      properties:
        id:
          type: integer
        name:
          type: string
        code:
          type: string
        default_language_id:
          type: integer
        created_at:
          type: string
        updated_at:
          type: string
    food_type:
      title: food_type
      type: object
      properties:
        id:
          type: integer
        name:
          type: string
        version:
          type: string
    language:
      title: language
      type: object
      properties:
        id:
          type: integer
        name:
          type: string
        native_name:
          type: string
        code:
          type: string
        created_at:
          type: string
        updated_at:
          type: string
    product:
      title: product
      type: object
      properties:
        id:
          type: integer
        name:
          type: string
        version:
          type: string
        created_at:
          type: string
        updated_at:
          type: string
    species:
      title: species
      type: object
      properties:
        id:
          type: integer
        name:
          type: string
        version:
          type: string
    timezone:
      title: timezone
      type: object
      properties:
        id:
          type: integer
        name:
          type: string
        timezone:
          type: string
        utc_offset:
          type: string
        created_at:
          type: string
        updated_at:
          type: string
    version:
      title: version
      type: object
      properties:
        server:
          type: string
        app:
          type: object
          properties:
            ios:
              type: object
              properties:
                min_version:
                  type: object
                  properties:
                    major:
                      type: string
                    minor:
                      type: string
                store_link:
                  type: string
            android:
              type: object
              properties:
                min_version:
                  type: object
                  properties:
                    major:
                      type: string
                    minor:
                      type: string
                store_link:
                  type: string
      x-examples:
        example-1:
          server: 1.0.1-35b2388
          app:
            ios:
              min_version:
                major: "1.0"
                minor: "1.17"
              store_link: "https://itunes.apple.com/gb/app/surehub/id1036175339?mt=8"
            android:
              min_version:
                major: 1
                minor: 0
              store_link: "https://play.google.com/store/apps/details?id=com.sureflap.surepetcare"
    photo:
      title: photo
      type: object
      properties:
        id:
          type: integer
        location:
          type: string
        uploading_user_id:
          type: integer
        version:
          type: string
        created_at:
          type: string
        updated_at:
          type: string
    position:
      title: position
      type: object
      properties:
        pet_id:
          type: integer
        tag_id:
          type: integer
        device_id:
          type: integer
        where:
          type: integer
        since:
          type: string
      x-examples:
        example_inside:
          pet_id: 12345
          tag_id: 67890
          device_id: 353424
          where: 1
          since: "2020-03-08T19:00:25+00:00"
      description: ""
  securitySchemes:
    BearerToken:
      type: http
      scheme: bearer

tags:
  - name: authentication
    description: The authentication methods. Of course, the first step EVER to follow.
  - name: device
    description: Some details about your devices
  - name: pet
    description: Pets operations
  - name: helper
    description: List of values, countries, languages, etc.
